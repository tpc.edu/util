[TOC]

# [Java]电子凭证-Java生成PDF

> **技术核心: `HTML`生成`PDF`**

## 背景

> 在某些业务场景中，需要提供相关的电子凭证，比如网银/支付宝中转账的电子回单，签约的电子合同等。方便用户查看，下载，打印。目前常用的解决方案是，把相关数据信息，生成对应的pdf文件返回给用户。

![电子凭证](https://static.oschina.net/uploads/space/2017/0507/202942_6jpZ_140593.png)

本文文档：https://my.oschina.net/lujianing/blog/894365
本文源码：http://git.oschina.net/lujianing/java_pdf_demo

## 实现思路

对于电子凭证的技术方案，总结如下:

1. `html`模板+`model`数据，通过`freemarker`进行渲染，便于维护和修改
2. 渲染后的`html`流，可通过`Flying Saucer`组件生成`pdf`文件流，或者将其转成`jpg`文件流
3. 在Web项目中，该文件流可以通过`ContentType`设置，在线查看/下载，不需通过附件服务

### 技术方案图

![流程图](https://static.oschina.net/uploads/space/2017/0507/200221_m8ZV_140593.png)

## Template Engines(模板引擎)

> 有多种模板引擎技术可以对`html`模板进行动态渲染, 请自行选择

### `Thymeleaf`

### `Apache Freemarker`

### `Groovy Templates`

### `velocity`

## `HTML TO PDF` 技术

`html`转换为`pdf`的关键技术是如何处理网页中复杂的`css`样式、以及中文乱码处理。

### 各实现对比表

![htmlTOpdf_技术对比图](image\htmlTOpdf_技术对比图.png)

![htmlTOpdf_技术对比图2](image\htmlTOpdf_技术对比图2.png)

> * 其他对比请参考[PDF技术（四）-Java实现Html转PDF文件](https://blog.csdn.net/qq_34190023/article/details/82999702)
> * 其他文件转PDF请参考[晋文子上的文章](https://blog.csdn.net/qq_34190023/category_8114387.html)

### WKhtmlTOpdf

`HTML`转化`PDF`步骤: 

1. 在[官网](https://wkhtmltopdf.org/)[下载](https://wkhtmltopdf.org/downloads.html) `wkhtmltopdf.exe`
2. 准备`html`文件路径或网址, `eg: /opt/template.html, https://www.baidu.com/`
3. 执行`wkhtmltopdf https://www.baidu.com/ baidu.pdf`代码实现参考该工具类

优质源码:
* [html2file](https://github.com/petterobam/html2file "收集一系列html转文档的开源插件，做成html页面转文件的微服务集成Web应用，目前包含 html转PDF、html转图片、html转markdown、html转word、excel等等。")

参考源码:
* [html2Pdf](https://gitee.com/spencercjh/myHtmlToPdfImage "基于wkhtmltopdf和PDFBOX的Html转PDF图片工具")
* [html2pdf](https://gitlab.com/tpc.edu/util)
* [htmltopdf-test](https://gitlab.com/micheljqh/htmltopdf-test "HTML TO PDF TEST (with wooio/htmltopdf-java)")
* [htmltopdf-java](https://github.com/wooio/htmltopdf-java "Convert HTML.String to pdf input stream")

### iText

iText是著名的开放源码的站点sourceforge一个项目，是用于生成PDF文档的一个java类库。通过iText不仅可以生成PDF或rtf的文档，而且可以将XML、Html文件转化为PDF文件。 
iText 官网：http://itextpdf.com/ 
iText 开发文档： http://developers.itextpdf.com/developers-home
iText Knowledge Base: https://kb.itextpdf.com/home
iText目前有两套版本iText5和iText7。iText5应该是网上用的比较多的一个版本。iText5因为是很多开发者参与贡献代码，因此在一些规范和设计上存在不合理的地方。iText7是后来官方针对iText5的重构，两个版本差别还是挺大的。不过在实际使用中，一般用到的都比较简单，所以不用特别拘泥于使用哪个版本。

功能简介:
* 最简单的例子 -> `com.itextpdf:itextpdf:5.5.11` -> `JavaToPdf`

* [iText-中文支持](http://developers.itextpdf.com/examples/font-examples/using-fonts#1227-tengwarquenya1.java) -> `JavaToPdfCN`

* [iText-Html渲染](http://developers.itextpdf.com/examples/xml-worker-itext5/xml-worker-examples) -> `com.itextpdf.tool:xmlworker:5.5.11` -> `JavaToPdfHtml`

* iText-Html-Freemarker渲染 -> `org.freemarker:freemarker:2.3.19` -> `JavaToPdfHtmlFreeMarker`

* Flying Saucer-CSS高级特性支持 -> `org.xhtmlrenderer:flying-saucer-pdf:9.1.5` `org.xhtmlrenderer:flying-saucer-pdf-itext5:9.1.5` -> `JavaToPdfHtmlFreeMarker`
> Flying Saucer is a pure-Java library for rendering arbitrary well-formed XML (or XHTML) using CSS 2.1 for layout and formatting, output to Swing panels, PDF, and images.
> Flying Saucer是基于iText的，支持对CSS高级特性的解析

* PDF转图片 -> `org.jpedal:jpedal-lgpl:4.74b27` -> `JavaToPdfImgHtmlFreeMarker`
> 在某些场景中，我们可能只需要返回图片格式的电子凭证，我们可以使用Jpedal组件，把pdf转成图片
> Jpedal支持将指定页Pdf生成图片，pdfDecoder.scaling设置图片的分辨率(不同分辨率下文件大小不同) ，支持多种图片格式，具体更多可自行研究

优质源码:
* [java_pdf_demo](https://gitee.com/lujianing/java_pdf_demo "Java生成pdf的demo")

### PhantomJS

纯前端解决方案, git地址: https://github.com/ariya/phantomjs 
PhantomJS 是一个基于 WebKit 的服务器端 JavaScript API。它全面支持web而不需浏览器支持，其快速，原生支持各种Web标准： DOM 处理, CSS 选择器, JSON, Canvas, 和 SVG。 PhantomJS 可以用于 页面自动化 ， 网络监测 ， 网页截屏 ，以及 无界面测试 等。

## 重点

* **[本文文档](https://my.oschina.net/lujianing/blog/894365 "电子凭证-Java生成Pdf")**
* **[java_pdf_demo](https://gitee.com/lujianing/java_pdf_demo "Java生成pdf的demo")**
* **[html2file](https://github.com/petterobam/html2file "收集一系列html转文档的开源插件，做成html页面转文件的微服务集成Web应用，目前包含 html转PDF、html转图片、html转markdown、html转word、excel等等。")**

