# StopWatch

## 概述

简单的秒表，允许对许多任务进行计时，暴露每个指定任务的总运行时间和运行时间。
隐藏`System.nanoTime()`的使用，提高应用程序代码的可读性，减少计算错误的可能性。
注意，此对象不是设计为线程安全的，也不使用同步。
该类通常用于在概念验证工作期间和开发中验证性能，而不是作为生产应用程序的一部分。

我当前了解的有两种StopWatch:
* `org.springframework:spring-core`中的`org.springframework.util.StopWatch`
* `org.apache.commons:commons-lang3`中的`org.apache.commons.lang3.time.StopWatch`

## 示例代码

### `org.springframework:spring-core`中的StopWatch

重要API, 可以参考`org.apache.commons.lang3.time.StopWatch`的注释及源码: 
```java
start();        //开始计时
split();        //设置split点
getSplitTime(); //获取从start 到 最后一次split的时间
reset();        //重置计时
suspend();      //暂停计时, 直到调用resume()后才恢复计时
resume();       //恢复计时
stop();         //停止计时
getTime();      //统计从start到现在的计时
```

* 引入jar
```xml
<!-- 下面两个jar包引入一个即可 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
	<version>2.4.3</version>
</dependency>
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-core</artifactId>
	<version>5.3.4</version>
</dependency>
```
* demo
```java
@Slf4j
public class StopWatchTest {

    @Test
    @SneakyThrows
    public void test() {
        org.springframework.util.StopWatch stopWatch = new org.springframework.util.StopWatch();
        stopWatch.start();

        // 统计sleep从start到stop经历的时间
        Thread.sleep(1000);
        stopWatch.stop();

        log.info("sleep elapsed time: {}", stopWatch.getLastTaskTimeMillis());
    }
}
```

### `org.apache.commons:commons-lang3`中的StopWatch

* 引入jar
```xml
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-lang3</artifactId>
    <version>3.11</version>
</dependency>
```
* demo
```java
@Slf4j
public class StopWatchTest {

    @Test
    @SneakyThrows
    public void test2() {
        org.apache.commons.lang3.time.StopWatch watch = org.apache.commons.lang3.time.StopWatch.createStarted();

        // 统计从start开始经历的时间
        Thread.sleep(1000);
        System.out.println(watch.getTime());

        // 统计计时点
        Thread.sleep(1000);
        watch.split();
        System.out.println(watch.getSplitTime());

        // 统计计时点
        Thread.sleep(1000);
        watch.split();
        System.out.println(watch.getSplitTime());

        // 复位后, 重新计时
        watch.reset();
        watch.start();
        Thread.sleep(1000);
        System.out.println(watch.getTime());

        // 暂停 与 恢复
        watch.suspend();
        System.out.println("暂停2秒钟");
        Thread.sleep(2000);

        watch.resume();
        Thread.sleep(1000);
        watch.stop();
        System.out.println(watch.getTime());
    }
}
```
* 运行结果
```tex
1000
2000
3000
1000
暂停2秒钟
2000
```


## 参考

* [参考文档](https://blog.csdn.net/yamaxifeng_132/article/details/84036250)
* [源码-util-demo](https://gitlab.com/tpc.edu/util)
