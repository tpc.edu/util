package com.example.util.common;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * @author LiuDong
 * @desc 任务执行时间监视器
 */
@Slf4j
public class StopWatchUtil {

    @SneakyThrows
    public static void main(String[] args) {
        stopWatch();
        stopWatch2();
    }

    /**
     * 支持多任务
     * org.springframework.util.StopWatch
     */
    @SneakyThrows
    private static void stopWatch() {
        org.springframework.util.StopWatch stopWatch = new org.springframework.util.StopWatch();
        stopWatch.start();

        // 统计sleep从start到stop经历的时间
        Thread.sleep(1000);
        stopWatch.stop();

        log.info("sleep elapsed time: {}", stopWatch.getLastTaskTimeMillis());
    }

    /**
     * 感觉更实用一些
     * org.apache.commons.lang3.time.StopWatch
     */
    @SneakyThrows
    private static void stopWatch2() {
        org.apache.commons.lang3.time.StopWatch watch = org.apache.commons.lang3.time.StopWatch.createStarted();

        // 统计从start开始经历的时间
        Thread.sleep(1000);
        System.out.println(watch.getTime());

        // 统计计时点
        Thread.sleep(1000);
        watch.split();
        System.out.println(watch.getSplitTime());

        // 统计计时点
        Thread.sleep(1000);
        watch.split();
        System.out.println(watch.getSplitTime());

        // 复位后, 重新计时
        watch.reset();
        watch.start();
        Thread.sleep(1000);
        System.out.println(watch.getTime());

        // 暂停 与 恢复
        watch.suspend();
        System.out.println("暂停2秒钟");
        Thread.sleep(2000);

        watch.resume();
        Thread.sleep(1000);
        watch.stop();
        System.out.println(watch.getTime());
    }

}
