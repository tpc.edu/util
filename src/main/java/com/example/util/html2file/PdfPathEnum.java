package com.example.util.html2file;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author LiuDong
 */
@Getter
@AllArgsConstructor
public enum PdfPathEnum {

    /** 路径名称 路径*/
    WK_HTML_TO_PDF_EXE_PATH("wkhtmltopdf.exe", "html2file/plugin/wkhtmltopdf.exe"),
    SRC_PATH("资源路径", "https://www.baidu.com/"),
    DEST_PATH("PDF保存路径", "html2file/pdf/"),
    SRC_PATH2("资源路径", "html2file/html/src02.html"),
    DEST_PATH2("PDF保存路径", "html2file/pdf/test02.pdf"),
    SRC_PATH3("资源路径", "html2file/html/src03.html"),
    DEST_PATH3("PDF保存路径", "html2file/pdf/test03.pdf"),
    ;

    private final String name;
    private final String path;
}
