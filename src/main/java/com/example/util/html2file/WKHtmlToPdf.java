package com.example.util.html2file;

import lombok.extern.slf4j.Slf4j;

import java.io.File;

/**
 * 使用 wkhtmltopdf.exe 将 html 转化为 PDF
 * html可以是磁盘上的文件, 也可以是网址
 * step 1: 下载 wkhtmltopdf.exe
 * step 2: 准备 html 文件路径或网址, eg: ${html_path}/template.html, https://www.baidu.com/
 * step 3: 执行 wkhtmltopdf https://www.baidu.com/ baidu.pdf; 代码实现参考该工具类
 * @author LiuDong
 */
@Slf4j
public class WKHtmlToPdf {

    private static final String CMD = PdfPathEnum.WK_HTML_TO_PDF_EXE_PATH.getPath() + " %s %s";

    /**
     * html转pdf
     *
     * @param srcPath  html路径，可以是硬盘上的路径，也可以是网络路径
     * @param destPath pdf保存路径
     * @return 转换成功返回true
     */
    public static boolean convert(String srcPath, String destPath) {
        //wkhtmltopdf在系统中的路径
        File file = new File(destPath);
        //如果pdf保存路径不存在，则创建路径
        boolean mkdirs = file.getParentFile().mkdirs();
        log.info("是否需要创建保存路径{}", mkdirs);
        try {
            Process proc = Runtime.getRuntime().exec(String.format(CMD, srcPath, destPath));
            proc.waitFor();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
