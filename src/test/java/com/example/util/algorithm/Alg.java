package com.example.util.algorithm;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Alg {
    /**
     * 滴滴面试算法题
     * 有N个孩子站成一排，每个孩子有一个分值。给这些孩子派发糖果，需要满足如下需求：
     * 1、每个孩子至少分到一个糖果
     * 2、分值更高的孩子比他相邻位的孩子获得更多的糖果
     * <p>
     * 求至少需要分发多少糖果？
     * <p>
     * 输入描述: 0,1,0             输出描述: 4
     * 输入示例: 5,4,1,1           输出示例: 7
     * 输入示例: 0，4，5，2，1，0  输出示例: 13
     */
    @Test
    public void sumTest() {
        System.out.println(Integer.compare(1, 1));
        System.out.println(Integer.compare(2, 4));
        System.out.println(Integer.compare(6, 3));
        System.out.println("======================================");
        System.out.println("[0]:" + get(Lists.newArrayList(0)));
        System.out.println("[0,0]:" + get(Lists.newArrayList(0, 0)));
        System.out.println("[0,1]:" + get(Lists.newArrayList(0, 1)));
        System.out.println("[1,0]:" + get(Lists.newArrayList(1, 0)));
        System.out.println("[0,1,0]:" + get(Lists.newArrayList(0, 1, 0)));
        System.out.println("[0,1,2]:" + get(Lists.newArrayList(0, 1, 2)));
        System.out.println("[2,1,0]:" + get(Lists.newArrayList(2, 1, 0)));
        System.out.println("[5,4,1,1]:" + get(Lists.newArrayList(5, 4, 1, 1)));
        System.out.println("[1,1,4,5]:" + get(Lists.newArrayList(1, 1, 4, 5)));
        System.out.println("[0,4,5,2,1,0]:" + get(Lists.newArrayList(0, 4, 5, 2, 1, 0)));
        System.out.println("[9,7,6,4,2,1]:" + get(Lists.newArrayList(9, 7, 6, 4, 2, 1)));
        System.out.println("[2,4,5,7,8,9]:" + get(Lists.newArrayList(2, 4, 5, 7, 8, 9)));
        System.out.println("[0,0,0,0,0,0,0,0]:" + get(Lists.newArrayList(0, 0, 0, 0, 0, 0, 0, 0)));
        System.out.println("[0,0,0,0,2,3,7,8]:" + get(Lists.newArrayList(0, 0, 0, 0, 2, 3, 7, 8)));
        System.out.println("[9,7,4,2,1,0,0,0]:" + get(Lists.newArrayList(9, 7, 4, 2, 1, 0, 0, 0)));
        System.out.println("[0,0,0,0,2,3,7,8,7]:" + get(Lists.newArrayList(0, 0, 0, 0, 2, 3, 7, 8, 7)));
        System.out.println("[6,9,7,4,2,1,0,0,0]:" + get(Lists.newArrayList(6, 9, 7, 4, 2, 1, 0, 0, 0)));
        System.out.println("[0,0,0,1,3,6,8,9,9,3,2,1,1,2,3,4,6,7,8,9,0,0,9,8,7,6,5,4,3,0]:" + get(Lists.newArrayList(0, 0, 0, 1, 3, 6, 8, 9, 9, 3, 2, 1, 1, 2, 3, 4, 6, 7, 8, 9, 0, 0, 9, 8, 7, 6, 5, 4, 3, 0)));
        System.out.println("[0,0,0,1,2,3,6,8,9,3,8,5,3,2,0,0,0,9,7,6,4,3,1,0,0,1,5,8,8,7,2,4,6,7,8,9,9,1,1,2,0,3,5,5,5,6,7,7,7,9,8,7,5,4,4,4,3,1,1,1,0]:" + get(Lists.newArrayList(0, 0, 0, 1, 2, 3, 6, 8, 9, 3, 8, 5, 3, 2, 0, 0, 0, 9, 7, 6, 4, 3, 1, 0, 0, 1, 5, 8, 8, 7, 2, 4, 6, 7, 8, 9, 9, 1, 1, 2, 0, 3, 5, 5, 5, 6, 7, 7, 7, 9, 8, 7, 5, 4, 4, 4, 3, 1, 1, 1, 0)));
        System.out.println("[0,0,0,1,2,3,6,8,9,3,8,5,3,2,0,0,0,9,7,6,4,3,1,0,0,1,5,8,8,7,2,4,6,7,8,9,9,1,1,2,0,3,5,5,5,6,7,9,9,9,8,7,5,4,4,4,3,1,1,1,0]:" + get(Lists.newArrayList(0, 0, 0, 1, 2, 3, 6, 8, 9, 3, 8, 5, 3, 2, 0, 0, 0, 9, 7, 6, 4, 3, 1, 0, 0, 1, 5, 8, 8, 7, 2, 4, 6, 7, 8, 9, 9, 1, 1, 2, 0, 3, 5, 5, 5, 6, 7, 9, 9, 9, 8, 7, 5, 4, 4, 4, 3, 1, 1, 1, 0)));
        ArrayList<Integer> i = Lists.newArrayList(0, 0, 0);
        Lists.newArrayList(1, 1, 1, 2, 3, 4, 5, 6, 6, 3, 2, 1, 1, 2, 3, 4, 5, 6, 7, 8, 1, 1, 8, 7, 6, 5, 4, 3, 2, 1).forEach(e -> i.set(0, i.get(0) + e));
        Lists.newArrayList(1, 1, 1, 2, 3, 4, 5, 6, 7, 1, 5, 4, 3, 2, 1, 1, 1, 7, 6, 5, 4, 3, 2, 1, 1, 2, 3, 4, 4, 2, 1, 2, 3, 4, 5, 6, 6, 1, 1, 2, 1, 2, 3, 3, 3, 4, 5, 5, 5, 8, 7, 6, 5, 4, 4, 4, 3, 2, 2, 2, 1).forEach(e -> i.set(1, i.get(1) + e));
        Lists.newArrayList(1, 1, 1, 2, 3, 4, 5, 6, 7, 1, 5, 4, 3, 2, 1, 1, 1, 7, 6, 5, 4, 3, 2, 1, 1, 2, 3, 4, 4, 2, 1, 2, 3, 4, 5, 6, 6, 1, 1, 2, 1, 2, 3, 3, 3, 4, 5, 8, 8, 8, 7, 6, 5, 4, 4, 4, 3, 2, 2, 2, 1).forEach(e -> i.set(2, i.get(2) + e));
        System.out.println(i);
    }

    private static int get(ArrayList<Integer> list) {
        ArrayList<Integer> sumList = Lists.newArrayList();
        if (list != null) {
            int index = 0;
            for (int i = 0; i < list.size(); i++) {
                if (i < index || (i != 0 && i == list.size() - 1)) {
                    continue;
                }
                int leftSum = 1;
                int leftFlag = -2;
                List<Map<String, Integer>> leftParts = Lists.newArrayList();
                for (int p = i - 1; p >= 0; p--) {
                    int compare = Integer.compare(list.get(p), list.get(p + 1));
                    // 趋势变更时结束循环
                    // 趋势不变时每次 num+1; 总趋势上下优先
                    // 何时初始化, 何时结束循环, 小趋势如何把控, 大趋势如何把控
                    if (p == i - 1) {
                        leftFlag = compare;
                    }
                    if (leftFlag != 0 && compare != 0 && leftFlag != compare) {
                        break;
                    } else {
                        if (compare != 0) {
                            leftSum++;
                            leftFlag = compare;
                        }
                        if (p == i - 1) {
                            leftParts.add(CommonUtils.initMap(compare, 2));
                        } else if (compare == leftParts.get(leftParts.size() - 1).get("flag")) {
                            leftParts.get(leftParts.size() - 1).put("sum", leftParts.get(leftParts.size() - 1).get("sum") + 1);
                        } else {
                            leftParts.add(CommonUtils.initMap(compare, 1));
                        }
                    }
                }
                int rightSum = 1;
                int rightFlag = -2;
                List<Map<String, Integer>> rightParts = Lists.newArrayList();
                for (int j = i + 1; j < list.size(); j++) {
                    int compare = Integer.compare(list.get(j - 1), list.get(j));
                    if (j == i + 1) {
                        rightFlag = compare;
                    }
                    if (rightFlag != 0 && compare != 0 && rightFlag != compare) {
                        break;
                    } else {
                        if (compare != 0) {
                            rightSum++;
                            rightFlag = compare;
                        }
                        if (j == i + 1) {
                            rightParts.add(CommonUtils.initMap(compare, 2));
                        } else if (compare == rightParts.get(rightParts.size() - 1).get("flag")) {
                            rightParts.get(rightParts.size() - 1).put("sum", rightParts.get(rightParts.size() - 1).get("sum") + 1);
                        } else {
                            rightParts.add(CommonUtils.initMap(compare, 1));
                        }
                    }
                }


                if (leftFlag == -2 && rightFlag == -2) {
                    sumList.add(1);
                } else {
                    int start = index;
                    for (Map<String, Integer> rightPart : rightParts) {
                        if (rightFlag == 0) {
                            for (int x = 0; x < rightPart.get("sum"); x++) {
                                sumList.add(1);
                            }
                        } else if (rightFlag < 0) {
                            for (int x = 0; x < rightPart.get("sum"); x++) {
                                if (start != 0 && x == 0 && sumList.size() == start + 1) {
                                    continue;
                                }
                                if (start == 0 && sumList.size() == 0) {
                                    sumList.add(1);
                                } else if (start != 0 && x == 1 && sumList.size() == start + 1) {
                                    sumList.add(2);
                                } else {
                                    if (rightPart.get("flag") == 0) {
                                        sumList.add(sumList.get(sumList.size() - 1));
                                    } else {
                                        sumList.add(sumList.get(sumList.size() - 1) + 1);
                                    }
                                }
                            }
                        } else {
                            for (int x = 0; x < rightPart.get("sum"); x++) {
                                if (start != 0 && x == 0 && sumList.size() == start + 1) {
                                    continue;
                                }
                                if (start == 0 && sumList.size() == 0) {
                                    sumList.add(rightSum);
                                } else if (start != 0 && x == 1 && sumList.size() == start + 1) {
                                    sumList.add(rightSum - 1);
                                } else {
                                    if (rightPart.get("flag") == 0) {
                                        sumList.add(sumList.get(sumList.size() - 1));
                                    } else {
                                        sumList.add(sumList.get(sumList.size() - 1) - 1);
                                    }
                                }
                            }
                        }
                        index += rightPart.get("sum");
                    }
                    if (leftSum < rightSum && rightFlag > 0 && leftParts.size() > 0) {
                        Map<String, Integer> leftPart = leftParts.get(0);
                        if (leftPart.get("flag") != 0) {
                            sumList.set(start, rightSum);
                        } else {
                            for (int x = 0; x < leftPart.get("sum"); x++) {
                                sumList.set(start - x, rightSum);
                            }
                        }
                    }
                    index--;
                }
            }
        }

        System.out.println(sumList.toString().replace(" ", ""));
        int sum = 0;
        if (sumList.size() > 0) {
            for (Integer tem : sumList) {
                sum += tem;
            }
        }
        return sum;
    }

}
