package com.example.util.algorithm;

import com.google.common.collect.Maps;

import java.util.Map;

public class CommonUtils {

    public static <E> Map<String, E> initMap(E a, E b) {
        Map<String, E> initMap = Maps.newHashMap();
        initMap.put("flag", a);
        initMap.put("sum", b);
        return initMap;
    }
}
