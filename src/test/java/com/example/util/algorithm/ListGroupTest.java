package com.example.util.algorithm;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author LiuDong44
 * @description desc
 * @date 2021/9/9 2:22
 */
@Slf4j
@SuppressWarnings("unchecked")
public class ListGroupTest {

    public static void main(String[] args) {
        List<List<Integer>> list = Lists.newArrayList(
                Lists.newArrayList(1,1,1,1),
                Lists.newArrayList(1,1,1,1,1,1,1,1,1,1),
                Lists.newArrayList(1,1,1,1,1,1,1,1,1,1,1,1),
                Lists.newArrayList(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),
                Lists.newArrayList(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),
                Lists.newArrayList(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)
        );
        list.forEach(ListGroupTest::g);
        System.out.println("==================================================================================");
        List<List<Integer>> list2 = Lists.newArrayList(
                Lists.newArrayList(1,3,2,1),
                Lists.newArrayList(1,6,1,8,1,7,1,9,1,1),
                Lists.newArrayList(1,2,1,3,6,1,9,1,5,1,2,1),
                Lists.newArrayList(1,1,6,1,1,1,8,1,1,1,2,1,1,8,1,4,1),
                Lists.newArrayList(1,1,3,1,2,1,1,7,6,1,3,1,7,1,1,1,1,1,1),
                Lists.newArrayList(1,2,3,6,5,1,2,1,1,3,1,1,9,1,1,7,1,4,6,1,2,1,1,9,1,1,1,1)
        );
        list2.forEach(ListGroupTest::g);
        System.out.println("==================================================================================");
        List<List<Integer>> list3 = Lists.newArrayList(
                Lists.newArrayList(11,3,2,1),
                Lists.newArrayList(1,51,1,1,1,7,1,9,1,1),
                Lists.newArrayList(11,15,18,3,61,1,29,1,5,1,2,1),
                Lists.newArrayList(1,12,6,15,1,16,8,18,1,1,25,18,1,8,1,4,1),
                Lists.newArrayList(13,16,3,1,22,2,1,71,76,16,3,1,57,1,13,1,1,13,12),
                Lists.newArrayList(1,2,3,6,5,1,2,1,1,3,1,1,9,1,1,7,1,4,6,1,2,1,1,9,1,1,1,1),
                Lists.newArrayList(11,15,1,2,3,18,3,1,2,3,6,5,1,2,1,1,3,1,1,13,10,3,1,22,2,1,71,76,16,3,9,1,1,7,1,4,6,1,2,1,1,9,1,1,1,11,76),
                Lists.newArrayList(10,15,1,2,3,18,3,10,2,3,6,5,1,2,1,1,3,1,1,13,10,3,1,22,2,1,71,76,16,3,9,1,1,7,1,4,6,1,2,10,1,9,1,1,1,11,10)
        );
        list3.forEach(ListGroupTest::g);
    }

    private static List<List<Integer>> g(List<Integer> list) {
        return g(list, t -> t);
    }

    private static final int P = 10;
    /**
     * 将一个集合从左往右进行分组, 且每组之和不大于10, 每组之和与前后相近元素相加大于10, 若元素本身大于10则将其独立成组;
     * @param list [1, 2, 1, 3, 6, 1, 9, 1, 5, 1, 2, 1]
     * @return     [[1, 2, 1, 3], [6, 1], [9, 1], [5, 1, 2, 1]]
     */
    private static <T> List<List<T>> g(List<T> list, Function<? super T, Integer> mapper) {
        if (CollUtil.isEmpty(list)) {
            return Lists.newArrayList();
        }
        List<Integer> intList = list.stream().map(mapper).collect(Collectors.toList());
        System.out.println(list.size() + ":" + intList.stream().reduce(0, Integer::sum) + ":" + intList);
        log.info("list元素个数:{}; list元素之和:{}; list:{}", list.size(), intList.stream().reduce(0, Integer::sum), intList);
        List<List<T>> page = Lists.newArrayList();
        int s = 0;
        int j = 0;
        for (int q = 0; q < list.size(); q++) {
            T t = list.get(q);
            int i = Optional.of(t).map(mapper).orElse(1);
            s += i;
            if (i > P) {
                s = 0;
                if (page.size() != 0 && page.get(j).size() == 0) {
                    page.get(j).add(t);
                } else {
                    if (page.size() != 0) {
                        j++;
                    }
                    page.add(Lists.newArrayList(t));
                }
                if (q < list.size() - 1) {
                    j++;
                    page.add(Lists.newArrayList());
                }
            } else {
                if (s > P) {
                    s = i;
                    j++;
                }
                if (page.size() < j + 1) {
                    page.add(Lists.newArrayList());
                }
                page.get(j).add(t);
            }
        }
        logPage(mapper, page);
        o(page, mapper);
        logPage(mapper, page);
        System.out.println("==================");
        return page;
    }

    private static <T> void logPage(Function<? super T, Integer> mapper, List<List<T>> page) {
        List<List<Integer>> intPage = page.stream().map(e -> e.stream().map(mapper).collect(Collectors.toList())).collect(Collectors.toList());
        List<Integer> collect = intPage.stream().map(e -> e.stream().reduce(0, Integer::sum)).collect(Collectors.toList());
        log.info("page元素个数:{}; page元素之和:{}; page:{}", page.size(), collect, intPage);
        System.out.println(page.size() + ":" + collect + ":" + intPage);
    }

    private static final int Q = 4;
    private static <T> List<List<T>> o(List<List<T>> page, Function<? super T, Integer> mapper) {
        if (CollUtil.isEmpty(page)) {
            return Lists.newArrayList();
        }
        if (page.get(page.size() - 1).stream().map(mapper).reduce(0, Integer::sum) > Q) {
            page.add(Lists.newArrayList());
        }
        return page;
    }
}
