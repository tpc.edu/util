package com.example.util.html2file;

import org.junit.jupiter.api.Test;

class WKHtmlToPdfTest {

    @Test
    void convert() {
        /*
           使用网址生成PDF
           执行 wkhtmltopdf https://www.baidu.com/ baidu.pdf;
         */
        String srcPath = PdfPathEnum.SRC_PATH.getPath();
        String destPath = PdfPathEnum.DEST_PATH.getPath() + "baidu.pdf";
        boolean convert = WKHtmlToPdf.convert(srcPath, destPath);
        System.out.println(convert);

        /*
           使用本地html文件生成PDF
           执行 wkhtmltopdf src02.html test02.pdf;
           执行 wkhtmltopdf src03.html test03.pdf;
         */
        boolean convert2 = WKHtmlToPdf.convert(PdfPathEnum.SRC_PATH2.getPath(), PdfPathEnum.DEST_PATH2.getPath());
        System.out.println(convert2);
        boolean convert3 = WKHtmlToPdf.convert(PdfPathEnum.SRC_PATH3.getPath(), PdfPathEnum.DEST_PATH3.getPath());
        System.out.println(convert3);

    }

}